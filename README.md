# Semi-meandric systems



## Information

This repository is for the study of semi-meandric systems related to the non-crossing pair partitions of the set $NC_2(2n)$.

<figure style="text-align: center !important;">
  <img src="Examples-of-semi-meanders-their-orders-n-and-their-winding-numbers-w.png" alt="semimeander"/>
  <figcaption style="font-style: italic !important;">Michael La Croix, University of Waterloo, Approaches to the Enumerative Theory of Meanders</figcaption>
</figure>

This code can generate and gather information on the set $NC_2(2n)$ in its entirety or by Monte Carlo approximation.

## Usage

This repository is comprised of a Jupyter notebook containing cells which can be run individually.

## Findings

The head of the sequence of interest:
```python
head = [1.0, 0.75, 0.6, 0.5178571428571429, 0.45714285714285713, 0.4166666666666667, 0.3832833832833833, 0.35882867132867136, 0.3374925727866904, 0.320969278399619, 0.306056172929238, 0.2940719766167336, 0.2830098263561717, 0.2738814213709893]
```

The head of the related triangular array:
```
tri = [[1],
       [1, 2],
       [3, 1, 5],
       [ 9,  3,  3, 14],
       [29,  9,  6,  9, 43],
       [ 96,  29,  19,  19,  29, 138],
       [330,  96,  60,  47,  60,  96, 462],
       [1151,  330,  200,  157,  157,  200,  330, 1580],
       [4105, 1151,  680,  516,  434,  516,  680, 1151, 5535],
       [14768,  4105,  2381,  1766,  1504,  1504,  1766,  2381,  4105, 19630],
       [53910, 14768,  8432,  6129,  5092,  4452,  5092,  6129,  8432, 14768, 70706],
       [197910,  53910,  30412,  21781,  17785,  15832,  15832,  17785, 21781,  30412,  53910, 256696],
       [734046, 197910, 110464,  78084,  62732,  54820,  49100,  54820, 62732,  78084, 110464, 197910, 942058],
       [2733224,  734046,  406254,  284270,  225626,  194554,  177932, 177932,  194554,  225626,  284270,  406254,  734046, 3476124],
       [10254712,  2733224,  1501136,  1040822,   817048,   695260, 626964,   570831,   626964,   695260,   817048,  1040822, 1501136,  2733224, 12929152],
       [38583603, 10254712,  5596880,  3852178,  2997872,  2525492, 2253760,  2098285,  2098285,  2253760,  2525492,  2997872, 3852178,  5596880, 10254712, 48278448],
       [146020409,  38583603,  20937004,  14313376,  11051064,   9222928, 8140872,   7497008,   6907290,   7497008,   8140872,   9222928, 11051064,  14313376,  20937004,  38583603, 181378079]]
```

Monte-Carlo estimate for $b_{5000} \approx 0.1165556$ ($1000$ samples).